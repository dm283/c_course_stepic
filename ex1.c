#include <stdio.h>
#include <limits.h>

void factorial_summ(int n);
int fctr(int i);

int main(){
    int n;
    scanf("%i", &n);
    factorial_summ(n);
    
    return 0;
}

void factorial_summ(int n){
    int res = 0;
    for(int i = 0; i <= n; i++){
        int fl;
        fl = fctr(i);
        if (res > INT_MAX-fl){
            printf("переполнение");
            res = 0;
            break;
        }
        res += fl;
    }
    if (res) printf("%d", res);
}

int fctr(int i){
    int res = 1;
    for (int j = 1; j <= i; j++){
        res *= j;
    }
    return res;
}
