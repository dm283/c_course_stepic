#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* merge(char* str1, char* str2);

int main()
{
    int str1_len;
    int str2_len;
    scanf("%i %i ", &str1_len, &str2_len);
    str1_len ++;
    str2_len ++;
    
    char* str1;
    char* str2;

    // Выделить память и считать строки
    str1 = malloc(str1_len * sizeof(char));
    str2 = malloc(str2_len * sizeof(char));

    scanf("%s", str1);
    scanf("%s", str2);

    printf("%s", merge(str1, str2));
}

char* merge(char* str1, char* str2)
{
    // Опишите функцию
    int p1=0, p2=0;
    int str3_len;
    char* str3;
    int common_len, mark;
    str3_len = strlen(str1) + strlen(str2);
    str3 = malloc(str3_len * sizeof(char));

    common_len = (strlen(str1) > strlen(str2)) ? strlen(str2) : strlen(str1);
    mark = (strlen(str1) > strlen(str2)) ? 1 : 2;
    
    for(int i=0; i<(common_len*2); i++){
        if (i%2){
            str3[i] = str2[p2++];
        }
        else{
            str3[i] = str1[p1++];
        }
    }

    if (mark==1){
        for(int i=common_len; i<strlen(str1); i++){
            str3[common_len+i] = str1[i];
        }
    }
    else if (mark==2){
        for(int i=common_len; i<strlen(str2); i++){
            str3[common_len+i] = str2[i];
        }
    }

    return str3;
}
