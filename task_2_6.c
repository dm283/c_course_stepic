#include <stdio.h>

unsigned char create_mask(int i);

int main(){
    unsigned char n; //8-bit number (0 to 255)
    int i;

    scanf("%hhu%d", &n, &i);
    //printf("n = %hhu\n", n);
    //printf("%ld\n", sizeof(n));
    //MASK = 7;
    n <<= i;
    n |= create_mask(i);
    printf("%hhu\n", n);

    return 0;
}

unsigned char create_mask(int i){
    unsigned char MASK=0;
    for (; i; i--) {
        MASK |= 1;
        //printf("MASK = %hu\n", MASK);
        //printf("i = %d\n", i); //
        if (i > 1) {
            MASK <<= 1;
        }
    }
    return MASK;
}