#include <stdio.h>

unsigned int ones (unsigned char n);
unsigned int zeros (unsigned char n);

int main(){
    unsigned char n;
    
    scanf("%hhd", &n);
    //printf("%ld\n", sizeof(n));

    // printf( "%u\n", ones(n) );
    printf( "%u\n", zeros(n) );

    return 0;
} 

unsigned int zeros (unsigned char n){
    unsigned int count = 0;
    unsigned int x = sizeof(n)*8;

    for(int i = 0; i < x; i++) {
        //printf("%d, %hhd, %hhd\n", i, n, !(n & 1));
        count += !(n & 1);
        n >>= 1;
    }

    return count;
}



unsigned int ones (unsigned char n){
    unsigned int count = 0;

    for (; n; n >>= 1)
        count += n & 1;

    // for (; n; count++)
    //     n &= (n - 1);
    
    return count;
}