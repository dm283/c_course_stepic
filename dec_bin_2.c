#include <stdio.h>

void init_binary();
void display_binary();
void dec_bin(int Decimal);

unsigned char bit[32];


int main(){
    int Decimal;

    init_binary();

    printf("Input decimal number: ");
    scanf("%d", &Decimal);

    dec_bin(Decimal);
    display_binary();
    
    return 0;
}


void dec_bin(int Decimal){
    // переделать на рекурсию!
    int Quotient, Reminder;

    Reminder = Decimal % 2;
    Quotient = Decimal / 2;
    bit[0] = Reminder;
    int i=1;
    while (Quotient){
        Reminder = Quotient % 2;
        Quotient /= 2;
        bit[i++] = Reminder;
    }
}


void init_binary(){
    for (int i = 0; i < 32; i++)
        bit[i] = 0;
}


void display_binary(){
    printf("\n");
    for (int i = 31; i >= 0; i--)
        printf("%hhd", bit[i]);
    printf("\n");
}
