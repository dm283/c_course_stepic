#include <stdio.h>
#define SIZE 20
//int a[20] = {2, -3, 3, 5, -1, 3, 6};
//int lmt = 7;
int a[SIZE];

int index_first_negative(int lmt);
int index_last_negative(int lmt);
int multi_between_negative(int lmt);
int multi_before_and_after_negative(int lmt);

int main(){
	int n;
	int lmt = 0;
	
	scanf("%d", &n); // input operation #
	/* input array */
	while ( (scanf("%d", &a[lmt])) ){
		lmt++;
		if (lmt >= SIZE) break;
	}
	
	/*for (int i=0; i<lmt; i++){
		printf("%d ", a[i]);
	}*/
	
	switch (n){
	case 0:
	    printf("%d\n", index_first_negative(lmt));
		break;
	case 1:
	    printf("%d\n", index_last_negative(lmt));
		break;
	case 2:
	    printf("%d\n", multi_between_negative(lmt));
		break;
	case 3:
	    printf("%d\n", multi_before_and_after_negative(lmt));
		break;
	default:
	    printf("Данные некорректны\n");
		break;
	}
	
	return 0;
}

int index_first_negative(int lmt){
	for(int i=0; i<lmt; i++){
		if (a[i] < 0){
			//printf("%d\n", i);
		    return i;
		}
	}
}

int index_last_negative(int lmt){
	for(int i=(lmt-1); i>=0; i--){
		if (a[i] < 0){
			//printf("%d\n", i);
		    return i;
		}
	}
}

int multi_between_negative(int lmt){
	int mlt = 1;
	for(int i=index_first_negative(lmt); i<index_last_negative(lmt); i++){
		mlt *= a[i];
	}
	//printf("%d\n", mlt);
	return mlt;
}

int multi_before_and_after_negative(int lmt){
	int mlt=1;
	for(int i=0; i<index_first_negative(lmt); i++){
		mlt *= a[i];
	}
	for(int i=index_last_negative(lmt); i<lmt; i++){
		mlt *= a[i];
	}
	//printf("%d\n", mlt);
	return mlt;
}
