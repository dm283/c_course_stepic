#include <stdio.h>
#include <limits.h>

int main(){
    unsigned char uc=1;
    unsigned int ui=1;

    printf("unsigned char size = %ld(bits)\n", sizeof(uc)*8); //8
    printf("unsigned int  size = %ld(bits)\n", sizeof(ui)*8); //32

    uc = 255; // 11111111
    printf("unsigned_chat_max = %hhu\n", uc);

    return 0;
}
