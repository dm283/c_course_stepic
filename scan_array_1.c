#include <stdio.h>

int main()
{
    int buf[1000];
    int tmp, i;
    while (1) {
        scanf("%d%c", &buf[i++], &tmp);
        if (tmp == '\n') {
            break;
    }
    }

    printf("i = %d\n", i);

    for (tmp = 0; tmp < i; tmp++){
        printf("%d ", buf[tmp]);
    }

    return 0;
}
