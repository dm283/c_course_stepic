#include <stdio.h>

unsigned short int create_mask(int i);

int main(){
    unsigned short int n; //16-bit number (0 to 65535)
    int i;

    scanf("%hu%d", &n, &i);
    //printf("n = %hu\n", n);
    //printf("%ld\n", sizeof(n));
    //MASK = 7;

    n &= create_mask(i);
    printf("%hu\n", n);

    return 0;
}

unsigned short int create_mask(int i){
    unsigned short int MASK=0;
    for (; i; i--) {
        MASK |= 1;
        //printf("MASK = %hu\n", MASK);
        //printf("i = %d\n", i); //
        if (i > 1) {
            MASK <<= 1;
        }
    }
    return MASK;
}