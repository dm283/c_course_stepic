#include <stdio.h>
#include <stdlib.h>
#define SIZE 100

int index_first_zero(int lmt);
int index_last_zero(int lmt);
int sum_between(int lmt);
int sum_before_and_after(int lmt);

int a[SIZE];
