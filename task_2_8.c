#include <stdio.h>

unsigned short int create_mask(int i);

int main(){
    unsigned short int n; //16-bit number (0 to 65535)
    int i;

    scanf("%hu%d", &n, &i);
    //printf("n = %hu\n", n);
    //printf("%ld\n", sizeof(n));
    //MASK = 7;

    n ^= create_mask(i);
    printf("%hu\n", n);

    return 0;
}

unsigned short int create_mask(int i){
    unsigned short int MASK=0;
    for (int j=15; j>=0; j--) {
        if (j != i) {
            MASK |= 1;
        }
        // else {
        //     MASK &= 65534;
        // }
        //printf("MASK = %hu\n", MASK);
        //printf("i = %d\n", i); //
        if (j > 0) {
            MASK <<= 1;
        }
    }

    // printf("MASK = %hu\n", MASK);
    return MASK;
}